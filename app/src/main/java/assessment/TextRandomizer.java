package assessment;

import java.util.Random;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

// This is created to easily randomize words in a text file, for testing purpose.

@UtilityClass
public class TextRandomizer {
    @SneakyThrows
    public String randomize(String text) {
        // randomize it
        String[] words = text.split("\\s+|(?=\n)");
        String[] randomizedWords = new String[words.length];
        Random random = new Random();

        for (int i = 0; i < words.length; i++) {
            if (i == words.length - 1) {
                randomizedWords[i] = words[words.length - 1];
            } else {
                int next = random.nextInt(i, words.length - 1);
                randomizedWords[i] = words[next];
                words[next] = words[i];
            }
        }
        return String.join(" ", randomizedWords);
    }
}
