package assessment;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import lombok.SneakyThrows;

public class TextRandomizerTest {
    @Test
    @SneakyThrows
    void shouldBeAbleToRandomizeText() {
        StringBuilder sb = new StringBuilder();
        try (InputStreamReader reader = new InputStreamReader(
                getClass().getClassLoader().getResourceAsStream("test-file-2.txt"));
                BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }
        String oriContent = sb.toString();
        String result = TextRandomizer
                .randomize(oriContent);
        assertNotEquals(oriContent, result);
        assertEquals(Stream.of(oriContent.split("\\s+|(?=\n)")).sorted().collect(Collectors.toList()),
                Stream.of(result.split("\\s+|(?=\n)")).sorted().collect(Collectors.toList()));
    }
}
