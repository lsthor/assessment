# Task

### Instructions
```
Please implement a command-line application that takes a path to a file as an argument and prints
a word count of its contents. The output should consist of a line for each word, with the number of
its occurrences in the file. It should be sorted by the number of occurrences starting with the most
frequent word.
Input file example:
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Non sodales neque
sodales ut. Gravida arcu ac tortor dignissim convallis aenean et tortor.
Ultricies mi eget mauris pharetra et ultrices neque. Vel eros donec ac odio
tempor orci.
Tellus in metus vulputate eu scelerisque felis. Volutpat diam ut venenatis
tellus in metus vulputate eu. Tempor id eu nisl nunc mi ipsum faucibus
vitae. Sit amet luctus venenatis lectus magna fringilla urna. Habitant
morbi tristique senectus et netus et. Eget felis eget nunc lobortis. Neque
vitae tempus quam pellentesque nec.
Pellentesque nec nam aliquam sem et tortor consequat id porta. Eget dolor
morbi non arcu risus quis. Lacinia quis vel eros donec. Velit laoreet id
donec ultrices tincidunt arcu non sodales. Feugiat in fermentum posuere
urna nec tincidunt. Tortor at risus viverra adipiscing at. In mollis nunc
sed id semper. Semper eget duis at tellus at urna condimentum mattis
pellentesque. Egestas purus viverra accumsan in nisl nisi scelerisque eu. A
cras semper auctor neque vitae tempus quam pellentesque.
Output example:
foo: 24
bar: 17
...
The results should be consistent between two files which contain the exact same words in the
same frequency but in a different order. Please provide the solution in an archive together with
instructions on how to build it and run it.
```

### Todo
- [x] a simple word count app that take in file name as parameter and show word counts in summary
- [x] try larger file
- [x] flags to ignore cases
- [x] remove punctuation mark
- [x] words must have at least a character or number
- [x] code cleanup

### How to run
Note: This project is build using JDK20 and Gradle. Please ensure you have JDK20 installed before running this.

#### Build from scratch
Build using Gradle command `./gradlew assemble`, and run with the following command `java -cp app/build/libs/app.jar assessment.App sample.txt`.

#### Run the pre-assembled jar file
run with the following command `java -cp app/build/libs/app.jar assessment.App sample.txt`.

### Usage help
```
Usage: java -cp app/build/libs/app.jar assessment.App [-c] [-m=<mode>] <file>
Word count
      <file>               The file that we want to perform word count on.
  -c, --case-insensitive   The flag to ignore case when doing word count.
  -m, --mode=<mode>        The mode to perform the count, supported values are
                             1 or 2. Default to 1 when not specify.
```

### Notes
- There are 2 modes of running the word count, default word counter and streaming word counter. When default word counter is used, the text file is read line by line into memory and will be counted. Where in streaming word count mode, the file will be read in smaller chunks and be processed straightaway, it is faster than the default one (I think)
- The code will only remove punctuation mark at the end of the word, not in front or in between.
